# os-course-reading-list

#### 介绍
SSE204 操作系统原理实验 大作业的论文阅读列表

#### 论文列表

| title | venue | year | link |
| --- | --- | --- | --- |
| Hrmonizing performance and isolation in microkernels with efficient intra-kernel isolation and communication. | ATC | 2020 | https://www.usenix.org/conference/atc20/presentation/gu |
| Snap: a microkernel approach to host networking. | SOSP | 2019 | https://dl.acm.org/doi/10.1145/3341301.3359657 |
| Determinizing Crash Behavior with a Verified Snapshot-Consistent Flash Translation Layer | OSDI | 2020 | https://www.usenix.org/conference/osdi20/presentation/chang |
| RedLeaf: Isolation and Communication in a Safe Operating System | OSDI | 2020 | https://www.usenix.org/conference/osdi20/presentation/narayanan-vikram |
| NrOS: Effective Replication and Sharing in an Operating System | OSDI | 2021 | https://www.usenix.org/conference/osdi21/presentation/bhardwaj |
| The benefits and costs of writing a POSIX kernel in a high-level language | OSDI | 2018 | https://www.usenix.org/conference/osdi18/presentation/cutler |
| Operating System Support for Safe and Efficient Auxiliary Execution | OSDI | 2022 | https://www.usenix.org/conference/osdi22/presentation/jing |
| wPerf: Generic Off-CPU Analysis to Identify Bottleneck Waiting Events | OSDI | 2018 | https://www.usenix.org/conference/osdi18/presentation/zhou |
| A fork() in the road | HotOS | 2019 | https://dl.acm.org/doi/10.1145/3317550.3321435 |
| Arachne: Core-Aware Thread Management | OSDI | 2018 | https://www.usenix.org/conference/osdi18/presentation/qin |
| Improving Interrupt Response Time in a Verifiable Protected Microkernel | EuroSys | 2012 | https://citeseerx.ist.psu.edu/doc/10.1.1.359.4537 |
| ffsck: The Fast File System Checker | FAST | 2013 | https://www.usenix.org/conference/fast13/technical-sessions/presentation/ma |
| F2FS: A New File System for Flash Storage | FAST | 2015 | http://www.usenix.org/conference/fast15/technical-sessions/presentation/lee |
| A Case Against (Most) Context Switches | HotOS | 2021 | https://dl.acm.org/doi/10.1145/3458336.3465274 |
| Zerializer: towards zero-copy serialization | HotOS | 2021 | https://dl.acm.org/doi/10.1145/3458336.3465283 |
| Unikernels: The Next Stage of Linux's Dominance | HotOS | 2019 | https://dl.acm.org/doi/10.1145/3317550.3321445 |
| I'm Not Dead Yet!: The Role of the Operating System in a Kernel-Bypass Era | HotOS | 2019 | https://dl.acm.org/doi/pdf/10.1145/3317550.3321422 |
| I/O Is Faster Than the CPU: Let's Partition Resources and Eliminate (Most) OS Abstractions | HotOS | 2019 | https://dl.acm.org/doi/pdf/10.1145/3317550.3321426 |
| Privbox: Faster System Calls Through Sandboxed Privileged Execution | ATC | 2022 | https://www.usenix.org/conference/atc22/presentation/kuznetsov |
| IPLFS: Log-Structured File System without Garbage Collection | ATC | 2022 | https://www.usenix.org/conference/atc22/presentation/kim-juwon |
| A Linux Kernel Implementation of the Homa Transport Protocol | ATC | 2021 | https://www.usenix.org/conference/atc21/presentation/ousterhout |
| SKQ: Event Scheduling for Optimizing Tail Latency in a Traditional OS Kernel | ATC | 2021 | https://www.usenix.org/conference/atc21/presentation/zhao-siyao |
| Fair Scheduling for AVX2 and AVX-512 Workloads | ATC | 2021 | https://www.usenix.org/conference/atc21/presentation/gottschlag |
